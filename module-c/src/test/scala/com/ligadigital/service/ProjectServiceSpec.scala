package com.ligadigital.service

import com.ligadigital.model.{Project, User}
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}
import org.mockito.Mockito._



class ProjectServiceSpec extends FlatSpec with Matchers with MockitoSugar {
  it should "create project if owner exists" in {
    val us = mock[UserService]
    val ps = new ProjectService(us)
    when(us.find("owner")).thenReturn(Some(User(1, "owner", isAdmin = false)))
    val res = ps.createProject("project1", "owner")
    res.isRight shouldEqual true
    res.right.get shouldEqual Project(1, "project1", "owner")
  }

  it should "not create project if owner does not exist" in {
    val us = mock[UserService]
    val ps = new ProjectService(us)
    when(us.find("owner")).thenReturn(None)
    val res = ps.createProject("project1", "owner")
    res.isLeft shouldEqual true
    res.left.get shouldEqual "NOT_FOUND"
  }
}
