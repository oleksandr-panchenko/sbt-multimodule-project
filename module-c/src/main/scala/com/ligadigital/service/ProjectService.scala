package com.ligadigital.service

import com.ligadigital.model.Project

class ProjectService(userService: UserService) {
  private var projects = List.empty[Project]

  def createProject(projectName: String, ownerName: String): Either[String, Project] = {
    if (userService.find(ownerName).isDefined) {
      val np = Project(projects.size + 1, projectName, ownerName)
      projects = np +: projects
      Right(np)
    } else {
      Left("NOT_FOUND")
    }
  }

}
