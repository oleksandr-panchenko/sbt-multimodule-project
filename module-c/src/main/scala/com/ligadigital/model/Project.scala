package com.ligadigital.model

case class Project(id: Long, name: String, owner: String)
