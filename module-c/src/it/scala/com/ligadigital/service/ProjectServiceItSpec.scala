package com.ligadigital.service

import com.ligadigital.model.Project
import org.scalatest.{FlatSpec, Matchers}

class ProjectServiceItSpec extends FlatSpec with Matchers {
  it should "create a new project" in {
    val us = new UserService
    val ps = new ProjectService(us)
    val res = ps.createProject("pr1", "user1")
    res.isRight shouldEqual true
    res.right.get shouldEqual Project(1, "pr1", "user1")
  }
}
