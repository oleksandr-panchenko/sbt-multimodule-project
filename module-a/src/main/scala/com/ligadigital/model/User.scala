package com.ligadigital.model

case class User(id: Long, name: String, isAdmin: Boolean)
