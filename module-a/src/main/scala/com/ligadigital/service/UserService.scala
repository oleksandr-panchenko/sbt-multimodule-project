package com.ligadigital.service

import com.ligadigital.model.User

class UserService {
  private var users = List(
    User(1, "user1", isAdmin = true),
    User(3, "user3", isAdmin = false),
    User(2, "user2", isAdmin = false)
  )

  def create(name: String, isAdmin: Boolean = false): Either[String, User] = {
    if (users.exists(_.name == name)) {
      Left("EXISTS")
    } else {
      val newUser = User(users.size + 1, name, isAdmin)
      users = newUser +: users
      Right(newUser)
    }
  }

  def find(name: String): Option[User] = {
    users.find(_.name == name)
  }

  def delete(name: String): Either[String, Unit] = {
    if (!users.exists(_.name == name)) {
      Left("NOT_EXISTS")
    } else {
      Right(())
    }
  }

}
