package com.ligadigital.service

import java.util.Date

import com.ligadigital.model.User
import org.scalatest.{EitherValues, FlatSpec, Matchers}

class UserServiceSpec extends FlatSpec with Matchers with EitherValues {
  it should "create a user with unique name" in {
    val userService = new UserService
    val timestamp = new Date().getTime
    val name = s"user$timestamp"
    val res = userService.create(name, isAdmin = true)
    res.isRight shouldEqual true
    userService.find(name) shouldEqual Some(User(res.right.value.id, name, isAdmin = true))
  }

  it should "not create a user with already existing name" in {
    val userService = new UserService
    val result = userService.create("user1")
    result.isLeft shouldEqual true
    result.left.exists(_ == "EXISTS") shouldEqual true
  }
}
