lazy val commonSettings = Seq(
  organization := "com.ligadigital",
  version := "0.1",
  scalaVersion := "2.12.5",
  parallelExecution in Test := false
)

lazy val root = (project in file("."))
  .aggregate(http, moduleC, moduleB, moduleA)
  .dependsOn(http, moduleC, moduleB, moduleA)

lazy val moduleA = (project in file("module-a"))
  .settings(
    commonSettings,
    name := "moduleA",
    libraryDependencies ++= commonDependencies
  )

lazy val moduleB = (project in file("module-b"))
  .settings(
    name := "moduleB",
    libraryDependencies ++= commonDependencies
  )

lazy val moduleC = (project in file("module-c"))
  .configs(IntegrationTest)
  .settings(
    name := "moduleC",
    Defaults.itSettings,
    libraryDependencies ++= commonDependencies ++ Seq(
      "org.scalatest" %% "scalatest" % "3.0.5" % "it, test",
      "org.mockito"  %  "mockito-core" % "2.18.0" % "it, test",
      "com.typesafe.akka" %% "akka-testkit" % libVersions.akka % Test
    )
  )
  .aggregate(moduleA)
  .dependsOn(moduleA)

lazy val http = (project in file("http-layer"))
  .settings(
    name := "httpModule",
    libraryDependencies ++= commonDependencies ++ Seq(
      "com.typesafe.akka" %% "akka-http" % libVersions.akkaHTTP,
      "com.typesafe.akka" %% "akka-slf4j" % libVersions.akka,
      "com.typesafe.akka" %% "akka-testkit" % libVersions.akka % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % libVersions.akkaHTTP % Test
    )
  )
  .aggregate(moduleC, moduleB, moduleA)
  .dependsOn(moduleC, moduleB, moduleA)

lazy val libVersions = new {
  val akka = "2.5.12"
  val akkaHTTP = "10.1.1"
  val logback = "1.1.2"
}

lazy val commonDependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % libVersions.akka,
  "ch.qos.logback" % "logback-core" % libVersions.logback,
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "org.mockito"  %  "mockito-core" % "2.18.0" % "test",
  "io.circe" % "circe-core_2.11" % "0.9.3"
)

