package com.ligadigital

import org.scalatest.{FlatSpec, Matchers}

/**
  * @author oleksandr.panchenko@ligadigital.com
  */
class CalculationServiceSpec extends FlatSpec with Matchers {
  it should "calculate plus operation" in {
    val s = new CalculationService
    s.calculate(1, 2) shouldEqual 3
  }
}
