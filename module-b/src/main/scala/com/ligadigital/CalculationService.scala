package com.ligadigital

class CalculationService {
  def calculate(a: Int, b: Int): Int = a + b
}
