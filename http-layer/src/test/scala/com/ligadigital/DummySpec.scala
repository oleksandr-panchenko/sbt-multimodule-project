package com.ligadigital

import org.scalatest.{FlatSpec, Matchers}

class DummySpec extends FlatSpec with Matchers {
  it should "be ok" in {
    1 + 2 shouldEqual 3
  }

  it should "be ok as well" in {
    false shouldEqual false
  }
}
